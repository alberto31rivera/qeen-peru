

<footer>
            <div class="footer-container" style="background-color: black;">
                <!--Footer Top Area Start-->
                <div class="footer-top-area ptb-50">
                    <div class="container">
                        <div class="row">
                            <!--Single Footer Start-->
                            <div class="col-lg-4 col-md-6">
                                <div class="single-footer">
                                    <!--Footer Logo Start-->
                                    <div class="footer-logo pt-4">
                                        <a href="index.html"><img src="{{ route('welcome') }}/public/img/logo/logo-noche.png" alt="Logo Store {{ config('app.name') }}"></a>
                                    </div>
                                    <!--Footer Logo End-->
                                    <!--Footer Content Start-->
                                    <div class="footer-content">
                                        <p style="color:white;">Marca 100% Peruana dedicada al diseño, confección y venta de prendas exclusivas.</p>
                                        <div class="contact">
                                            <p style="color:white;"><label>Dirección: </label>Jr gamarra 1160 Tda. 1601 piso 16</p>
                                            <p style="color:white;"><label>Celular: </label><a href="tel:+51951162916"></a>  951 162 916 / 948 631 509  
                                            </p>
                                            <!-- <p><label>Email:</label><a
                                                    href="mailto:Support@demo.com">Support@demo.com</a></p> -->
                                        </div>
                                    </div>
                                    <!--Footer Content End-->
                                </div>
                            </div>
                            <!--Single Footer End-->
                            <!--Single Footer Start-->
                            <div class="col-lg-2 col-md-6">
                                <div class="single-footer mt-30">
                                    <div class="footer-title">
                                        <h3 style="color:white;">Información</h3>
                                    </div>
                                    <ul class="footer-info">
                                        <li><a href="#">Nosotros</a></li>
                                        <li><a href="#">Contacto</a></li>
                                        <li><a href="#">Tienda</a></li>
                                        <li><a href="#">Blog</a></li>
                                        
                                       
                                    </ul>
                                </div>
                            </div>
                            <!--Single Footer End-->
                            <!--Single Footer Start-->
                            <div class="col-lg-2 col-md-6">
                                <div class="single-footer mt-30">
                                    <div class="footer-title">
                                        <h3 style="color:white;">Mi Cuenta</h3>
                                    </div>
                                    <ul class="footer-info">
                                        <li><a href="#">Mi cuenta</a></li>
                                        <li><a href="#">Mis Pedidos</a></li>
                                        <li><a href="#">Tienda</a></li>
                                        <li><a href="#">Ofertas</a></li>
                                      
                                    </ul>
                                </div>
                            </div>
                            <!--Single Footer End-->
                            <!--Single Footer Start-->
                            <div class="col-lg-4 col-md-6">
                                <div class="single-footer mt-30">
                                    <div class="footer-title">
                                        <h3 style="color:white;">Síguenos</h3>
                                    </div>
                                    <ul class="socil-icon mb-40">
                                         
                                        <li><a target="_blank" href="https://www.facebook.com/LuneySoleil1" data-toggle="tooltip" title="Facebook"><i
                                                    class="ion-social-facebook"></i></a></li>
                                        <li><a target="_blank" href="https://www.instagram.com/luneysoleilperu/" data-toggle="tooltip" title="Instagram"><i
                                                    class="ion-social-instagram"></i></a></li>            
                                        
                                         
                                        <li><a target="_blank" href="https://www.tiktok.com/@luneysoleil"><img src="{{ route('welcome') }}/public/img/tiktok.ico" width="16" height="16" alt="tiktok"></a></li>
                                        <li><a target="_blank" href="#" data-toggle="tooltip" title="Rss"><i
                                                    class="ion-social-rss"></i></a></li>
                                    </ul>
                                    <div class="footer-title">
                                        <h3>Download Apps</h3>
                                    </div>
                                    <div class="footer-content">
                                        <a href="#"><img src="{{ asset('img/apps/1.png') }}" alt="Apps Google"></a>
                                        <a href="#"><img src="{{ asset('img/apps/2.png') }}" alt="Apps Google"></a>
                                    </div>
                                </div>
                            </div>
                            <!--Single Footer End-->
                        </div>
                    </div>
                </div>
                <!--Footer Top Area End-->

                <!--Footer Bottom Area Start-->
                <div class="footer-bottom-area">
                    <div class="container">
                        <div class="row">
                            <!--Footer Left Content Start-->
                            <div class="col-lg-6 col-md-6">
                                <div class="copyright-text">
                                    <p>Copyright ©
                                        <script>
                                            document.write(new Date().getFullYear());
                                        </script> <a href="https://wa.link/0oyz4d" target="_blank">E
                                            Rivera</a>, All Rights Reserved.
                                    </p>
                                </div>
                            </div>
                            <!--Footer Left Content End-->
                            <!--Footer Right Content Start-->
                            <div class="col-lg-6 col-md-6">
                                <div class="payment-img text-right">
                                    <a href="#"><img src="{{ asset('img/payment/payment.png') }}" alt="botón de pagos"></a>
                                </div>
                            </div>
                            <!--Footer Right Content End-->
                        </div>
                    </div>
                </div>
                <!--Footer Bottom Area End-->
            </div>
        </footer>