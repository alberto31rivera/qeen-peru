@extends('admin.layouts.app')

{{-- @section('titulo')
    GESTION DE TALLAS
@endsection --}}

@section('contenido')
    <div class="panel panel-default">
        <div class="panel-heading">
            Listado de Tallas

        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
            @if ($message = Session::get('success'))
                                <div class="alert alert-success alert-block mt-20">
                                    <button type="button" class="close" data-dismiss="alert">×</button>
                                    <strong> {{ $message }} </strong>
                                </div>
                            @endif
            <div class="col-md-3">
                <h4>Crear / Editar Talla</h4>
                <form action="{{route('storeTalla')}}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="">Talla</label>
                        <input type="text" class="form-control" id="talla" name="talla">
                        <input type="hidden" name="idTalla" id="idTalla" value="0">
                    </div>

                    <button type="submit" class="btn btn-success form-control"> Guardar </button>
                </form>
            </div>
            <div class="table-responsive col-md-9">
                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Talla</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($tallas as $talla)
                            <tr class="odd gradeX">
                                <td>{{ $talla->id }}</td>
                                <td>{{ $talla->talla }}</td>
                                <td width="30px">
                                    <a href=""><i class="fa fa-edit text-primary"></i></a>
                                    <a href=""><i class="fa fa-trash text-danger"></i></a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /.table-responsive -->

        </div>
        <!-- /.panel-body -->
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            $('#dataTables-example').DataTable({
                responsive: true,
                language: {
                    search: "Buscar",
                    paginate: {
                        first: "Primera",
                        previous: "Anterior",
                        next: "Siguiente",
                        last: "Ultima"
                    },
                    info: "Mostrando _START_ de _END_ en _TOTAL_ registros",
                    lengthMenu:     "Mostrar  _MENU_  Registros",
                }
            });
        });
    </script>
@endsection
