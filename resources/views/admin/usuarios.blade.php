@extends('admin.layouts.app')

{{-- @section('titulo')
    GESTION DE TALLAS
@endsection --}}

@section('contenido')
    <div class="panel panel-default">
        <div class="panel-heading">
            Listado de Usuarios

            <a href="{{route('crearUsuario')}}" class="btn btn-success">  <i class="fa fa-plus-circle"></i> </a>
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
            @if ($message = Session::get('success'))
                                <div class="alert alert-success alert-block mt-20">
                                    <button type="button" class="close" data-dismiss="alert">×</button>
                                    <strong> {{ $message }} </strong>
                                </div>
                            @endif

            <div class="table-responsive col-md-12">
                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Nombre</th>
                            <th>Usuario</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($usuarios as $usuario)
                            <tr class="odd gradeX">
                                <td>{{ $usuario->id }}</td>
                                <td>{{ $usuario->name }}</td>
                                <td>{{ $usuario->email }}</td>
                                <td width="30px">
                                    <a href=""><i class="fa fa-edit text-primary"></i></a>
                                    <a href=""><i class="fa fa-trash text-danger"></i></a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /.table-responsive -->

        </div>
        <!-- /.panel-body -->
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            $('#dataTables-example').DataTable({
                responsive: true,
                language: {
                    search: "Buscar",
                    paginate: {
                        first: "Primera",
                        previous: "Anterior",
                        next: "Siguiente",
                        last: "Ultima"
                    },
                    info: "Mostrando _START_ de _END_ en _TOTAL_ registros",
                    lengthMenu:     "Mostrar  _MENU_  Registros",
                }
            });
        });
    </script>
@endsection
