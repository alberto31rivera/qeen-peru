<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Pagar</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Place favicon.ico in the root directory -->
    @include('layouts.rels')

    <style>
        .custom-input-file {
            background-color: #941B80;
            color: #fff;
            cursor: pointer;
            font-size: 12px;
            font-weight: bold;
            margin: 0 auto 0;
            min-height: 15px;
            overflow: hidden;
            padding: 10px;
            position: relative;
            text-align: center;
            width: 400px;
            margin-top: 20px;
        }

        .custom-input-file .input-file {
            border: 10000px solid transparent;
            cursor: pointer;
            font-size: 10000px;
            margin: 0;
            opacity: 0;
            outline: 0 none;
            padding: 0;
            position: absolute;
            right: -1000px;
            top: -1000px;
        }

    </style>

</head>

<body>
    <!--[if lt IE 8]>
 <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
 <![endif]-->

    <div class="wrapper home-3">
        <!--Header Area Start-->
        @include('layouts.header')
        <!--Header Area End-->

        <!--Checkout Area Start-->
        <div class="checkout-area mt-20">
            <div class="container">
                <div class="row">
                    @if (Auth::user() == null)
                        <!--Coupon Area Start-->
                        <div class="col-lg-12">
                            @if ($message = Session::get('success'))
                                <div class="alert alert-danger alert-block">
                                    <button type="button" class="close" data-dismiss="alert">×</button>
                                    <strong> {{ $message }} </strong>
                                </div>
                            @endif
                            <div class="coupon-accordion">
                                <!--Accrodion Start-->
                                <h3>Ya Eres cliente? <span id="showlogin">Inicia sesión</span></h3>
                                <div id="checkout-login" class="coupon-content">
                                    <div class="login-form mt-0">
                                        <form action="{{ route('login_carrito') }}" method="POST" >
                                            @csrf
                                            <p>Inicia sesión</p>
                                            <div class="form-fild">
                                                <p><label>Correo<span class="required">*</span></label></p>
                                                <input type="text" name="username" value="">
                                            </div>
                                            <div class="form-fild">
                                                <p><label>Clave <span class="required">*</span></label></p>
                                                <input type="password" name="password" value="">
                                                <input type="hidden" name="idPedido" value="{{ $pedido->id }}">
                                            </div>
                                            <div class="login-submit">
                                                <button type="submit" class="form-button">Enviar</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <!--Accrodion End-->

                            </div>
                        </div>
                        <!--Coupon Area End-->
                    @endif
                    <!--Checkout Area Start-->
                    <div class="col-md-12">
                        <div class="checkout-form-area">
                            <div class="checkout-title">
                                <h3>Detalles de compra</h3>
                            </div>
                            <div class="ceckout-form">
                                <form action="{{route('pagoYape')}}" accept-charset="UTF-8" enctype="multipart/form-data" method="post">
                                    @csrf
                                    <!--Billing Fields Start-->
                                    <div class="billing-fields">
                                        <div class="form-fild first-name">
                                            <p><label>Nombre<span class="required">*</span></label></p>
                                            <input type="text" placeholder="" name="name" id="name" value="">
                                        </div>
                                        <div class="form-fild last-name">
                                            <p><label>Apellido<span class="required">*</span></label></p>
                                            <input type="text" placeholder="" name="apellido" id="apellido" value="">
                                        </div>

                                        <div class="form-fild billing_address_1">
                                            <p><label>Dirección<span class="required">*</span></label></p>
                                            <input type="text" placeholder="Direccion" name="direccion" id="direccion"
                                                value="">
                                        </div>

                                        <div class="form-fild billing_address_2">
                                        <label>Referencia </label>
                                        <input type="text" placeholder="referencia.."
                                                name="billing_company_name" id="edificio" value="">
                                        </div>
                                        <div class="row">

                                        <div class="col-md-6 py-2">
                                            <p><label>Departamento<span class="required">*</span></label></p>
                                            <select name="departamento" id="departamento" class="form-control" onchange="getProvincias()">
                                                <option value="">SELECCIONE</option>
                                                @foreach ($departamentos as $departamento)
                                                    <option value="{{ $departamento->id }}">
                                                        {{ $departamento->name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="col-md-6 py-2">
                                            <p><label>Provincia<span class="required">*</span></label></p>
                                            <select name="provincia" id="provincia" class="form-control" onchange="getDistritos()">
                                                <option value="">SELECCIONE</option>
                                            </select>
                                        </div>

                                        <div class="col-md-6 py-2">
                                            <p><label>Distrito<span class="required">*</span></label></p>
                                            <select name="distrito" class="form-control" id="distrito">
                                                <option value="">SELECCIONE</option>
                                            </select>
                                        </div>

                                        <div class="col-md-6 py-2">
                                            <p><label>Tipo de Envio: <span class="required">*</span></label></p>
                                            <select name="tipoEnvio" class="form-control" id="tipoEnvio">
                                                <option value="">SELECCIONE</option>
                                                <option value="1">RECOJO EN TIENDA</option>
                                                <option value="2">DELIVERY</option>
                                            </select>
                                        </div>
                                        </div>

                                            
                                        <div class="form-fild billing_phone" style="width: 49%;padding-top:15px;">
                                            <p><label>Telefono<span class="required">*</span></label></p>
                                            <input type="text" placeholder="" id="telefono" name="telefono" value="">
                                        </div>
                                        <div class="form-fild email" style="width: 49%;padding-top:15px;">
                                            <p><label>Correo<span class="required">*</span></label></p>
                                            @if(isset(Auth::user()->email))
                                            <input type="hidden" placeholder="" id="correo" name="correo" value="{{Auth::user()->email}}">
                                            <input type="text" placeholder="" id="correo" disabled name="" value="{{Auth::user()->email}}">
                                            @else
                                            <input type="text" placeholder="" id="correo" name="correo" value="">
                                            @endif
                                        </div>
                                    </div>
                                    <!--Billing Fields End-->


                                    <!--Your Order Fields Start-->
                                    <div class="your-order-fields mt-30">
                                        <div class="your-order-title">
                                            <h3>Tu Orden</h3>
                                        </div>
                                        <div class="your-order-table table-responsive">
                                            <table>
                                                <thead>
                                                    <tr>
                                                        <th class="product-name">Producto</th>
                                                        <th class="product-total">Total</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($pedido->relacionpedido as $producto)
                                                        <tr class="cart_item">
                                                            <td class="product-name">
                                                                {{ $producto->detalle->titulo }}
                                                                <strong class="product-quantity"> ×
                                                                    {{ $producto->cantidad }}</strong>
                                                            </td>
                                                            <td class="product-total"><span class="amount">S/
                                                                    {{ $producto->cantidad * $producto->detalle->precio }}</span>
                                                            </td>
                                                        </tr>
                                                    @endforeach


                                                </tbody>
                                                <tfoot>
                                                    <tr class="order-total">
                                                        <th>Total</th>
                                                        <td><strong><span class="total-amount">S/
                                                                    {{ round($pedido->total) }}</span></strong>
                                                        </td>
                                                        <input type="hidden" id="TotalPedido" name="TotalPedido"
                                                            value="{{ round($pedido->total) }}">
                                                            <input type="hidden" id="IdPedido" name="idPedido"
                                                            value="{{ $pedido->id }}">
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                    <!--Your Order Fields End-->
                                    <div class="checkout-payment">
                                        <div class="your-order-title">
                                            <h3>Seleccione:</h3>
                                        </div>

                                        <ul>
                                            <li class="payment_method_cheque-li">
                                                <input id="payment_method_cheque" class="input-radio"
                                                    name="payment_method" value="bacs" type="radio">
                                                <label for="payment_method_cheque">Pagar Con Yape / Plin / Transferencia Bancaria</label>
                                                <div class="pay-box payment_method_cheque" style="display: none">
                                                    <div class="row mt-3">
                                                        <div class="col-md-4 py-2"> 
                                                            <img class="mx-auto d-block" src="{{ asset('img/payment/yape.jpg') }}" width="70%" alt="">
                                                        </div>
                                                        <div class="col-md-4 py-2"> 
                                                            <img class="mx-auto d-block" src="{{ asset('img/payment/plin.jpg') }}" width="70%" alt="">
                                                        </div>
                                                        <div class="col-md-4 py-2"> 
                                                            <img class="mx-auto d-block" src="{{ asset('img/payment/banco.jpg') }}" width="70%" alt="">
                                                        </div>

                                                        <div class="custom-input-file col-md-6 col-sm-6 col-xs-6">
                                                            <input type="file" id="fichero-tarifas"
                                                                class="input-file" required value="" name="comprobante">
                                                            Subir Comprobante
                                                        </div>
                                                    
                                                    


                                                        <div class="container">
                                                            <div class="row justify-content-md-center">
                                                                <div class="col col-lg-2">
                                                                 
                                                                </div>
                                                                <div class="col-md-auto">
                                                                <button class="order-btn mt-5" type="submit">Enviar</button>
                                                                </div>
                                                                <div class="col col-lg-2">
                                                                
                                                                </div>
                                                            </div> 
                                                        </div>


                                                    </div>
                                                </div>
                                            </li>



                                            <li class="payment_method_paypal-li">
                                                <input id="payment_method_paypal" class="input-rado"
                                                    name="payment_method" value="paypal"
                                                    data-order_button_text="Pagar con Tarjeta de Débito o Crédito"
                                                    type="radio">
                                                <label for="payment_method_paypal"> Pagar con Tarjeta de Débito o
                                                    Crédito </label>
                                                <div class="pay-box payment_method_paypal">
                                                    <button class="btn btn-success btn-lg w-25 form-control mt-3"
                                                        id="buyButton">Pagar</button>
                                                </div>
                                            </li>
                                        </ul>
                                        {{-- <button class="order-btn" type="submit">Realizar Orden</button> --}}
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!--Checkout Area End-->
                </div>
            </div>
        </div>
        <!--Checkout Area End-->

        <!--Footer Area Start-->
        @include('layouts.footer')
        <!--Footer Area End-->
    </div>



    <!--All Js Here-->

    <!--Jquery 3.6.0-->
    <script src="{{ asset('js/vendor/modernizr-3.6.0.min.js') }}"></script>
    <script src="{{ asset('js/vendor/jquery-3.6.0.min.js') }}"></script>
    <script src="{{ asset('js/vendor/jquery-migrate-3.3.2.min.js') }}"></script>
    <!--Popper-->
    <script src="{{ asset('js/popper.min.js') }}"></script>
    <!--Bootstrap-->
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <!--Imagesloaded-->
    <script src="{{ asset('js/imagesloaded.pkgd.min.js') }}"></script>
    <!--Isotope-->
    <script src="{{ asset('js/isotope.pkgd.min.js') }}"></script>
    <!--Ui js-->
    <script src="{{ asset('js/jquery-ui.min.js') }}"></script>
    <!--Countdown-->
    <script src="{{ asset('js/jquery.countdown.min.js') }}"></script>
    <!--Counterup-->
    <script src="{{ asset('js/jquery.counterup.min.js') }}"></script>
    <!--ScrollUp-->
    <script src="{{ asset('js/jquery.scrollUp.min.js') }}"></script>
    <!--Chosen js-->
    <script src="{{ asset('js/chosen.jquery.js') }}"></script>
    <!--Meanmenu js-->
    <script src="{{ asset('js/jquery.meanmenu.min.js') }}"></script>
    <!--Instafeed-->
    <script src="{{ asset('js/instafeed.min.js') }}"></script>
    <!--EasyZoom-->
    <script src="{{ asset('js/easyzoom.min.js') }}"></script>
    <!--Fancybox-->
    <script src="{{ asset('js/jquery.fancybox.pack.js') }}"></script>
    <!--Nivo Slider-->
    <script src="{{ asset('js/jquery.nivo.slider.js') }}"></script>
    <!--Waypoints-->
    <script src="{{ asset('js/waypoints.min.js') }}"></script>
    <!--Carousel-->
    <script src="{{ asset('js/owl.carousel.min.js') }}"></script>
    <!--Slick-->
    <script src="{{ asset('js/slick.min.js') }}"></script>
    <!--Wow-->
    <script src="{{ asset('js/wow.min.js') }}"></script>
    <!--Plugins-->
    <script src="{{ asset('js/plugins.js') }}"></script>
    <!--Main Js-->
    <script src="{{ asset('js/main.js') }}"></script>
    <script src="{{ asset('jquery.flexslider.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <script src="https://checkout.culqi.com/js/v3"></script>
    <script>
        const total = parseInt($("#TotalPedido").val()) * 100;
        Culqi.publicKey = 'pk_live_20f46b502f48e089';

        Culqi.settings({
            title: 'Tienda',
            currency: 'PEN',
            description: 'Pedido de Tienda Completo',
            amount: total
        });

        $('#buyButton').on('click', function(e) {
            Culqi.open();
            e.preventDefault();
        });

        function culqi() {
            if (Culqi.token) { // ¡Objeto Token creado exitosamente!
                var token = Culqi.token.id;
                const params = {
                    tokenCulqi: token,
                    name: $("#name").val(),
                    apellido: $("#apellido").val(),
                    departamento: $("#departamento").val(),
                    provincia: $("#provincia").val(),
                    distrito: $("#distrito").val(),
                    direccion: $("#direccion").val(),
                    edificio: $("#edificio").val(),
                    telefono: $("#telefono").val(),
                    correo: $("#correo").val(),
                    idPedido: $("#IdPedido").val(),
                    tipoEnvio: $("#tipoEnvio").val()
                }

                axios.post('/checkout/pago', params).then((response) => {
                    console.log(response.data)
                    if(response.data == "Venta Exitosa")
                    {
                        window.location.href = "/cuenta"
                    }
                })
            } else { // ¡Hubo algún problema!
                console.log(Culqi.error);
                alert(Culqi.error.user_message);
            }
        };


        function getProvincias()
        {
            axios.get('/provincias/'+$("#departamento").val()).then((response) => {
                var provincias = $("#provincia");
                provincias.html(response.data.html)
            })
        }

        function getDistritos()
        {
            axios.get('/distritos/'+$("#provincia").val()).then((response) => {
                var distrito = $("#distrito");
                distrito.html(response.data.html)
            })
        }
    </script>
</body>

</html>
