<?php

namespace App\Http\Controllers;

use App\Models\{Cupon, Distrito, Deprtamento, Provincia, User};

class DireccionesController extends Controller
{
    public function provincias($idDepartamento)
    {
        if($idDepartamento < 10)
        {
            $idDepartamento = "0".$idDepartamento;
        }
        $provincias = Provincia::whereDepartmentId($idDepartamento)->get();

        $html = "<option value=''>SELECCIONE</option>";
        foreach($provincias as $provincia)
        {
            $html .= "<option value='$provincia->id'>$provincia->name</option>";
        }
        return response()->json([
            'data' => $provincias,
            'html' => $html
        ]);
    }

    public function distritos($idProvincia)
    {
        if($idProvincia < 1000)
        {
            $idProvincia = "0".$idProvincia;
        }
        $distritos = Distrito::whereProvinceId($idProvincia)->get();

        $html = "<option value=''>SELECCIONE</option>";
        foreach ($distritos as $distrito ) {
            $html .= "<option value='$distrito->id'>$distrito->name</option>";
        }
        return response()->json([
            'data' => $distritos,
            'html' => $html
        ]);
    }
}
