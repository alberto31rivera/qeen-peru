<?php

namespace App\Http\Controllers;

use App\Models\Talla;
use Illuminate\Http\Request;

class TallaController extends Controller
{

    public function index()
    {
        return view('admin.tallas', ['tallas' => Talla::all()]);
    }

    public function store(Request $request)
    {
        Talla::updateOrCreate(
            [
                'talla' => $request->talla
            ],
            [
                'talla' => $request->talla
            ]
        );

        return back()->with('success', 'Talla Creada / Actualizada con exito');
    }
}
