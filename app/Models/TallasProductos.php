<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TallasProductos extends Model
{
    use HasFactory;

    protected $fillable = [
        'id_producto',
        'id_talla'
    ];

    public function detalle()
    {
        return $this->hasOne(Talla::class, 'id', 'id_talla');
    }
}
