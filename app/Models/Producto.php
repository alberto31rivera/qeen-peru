<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    use HasFactory;

    protected $fillable = [
        'precio', 'marca', 'talla'
    ];

    public function imagenes()
    {
        return $this->hasOne(ImagenProducto::class, 'id_producto', 'id');
    }

    public function atributo()
    {
        return $this->hasMany(AtributosProducto::class, 'producto_id', 'id');
    }

    //Scope
    public function scopeTalla($query, $talla)
    {
        if ($talla)
            return $query->where($talla, 1);
    }

    public function scopeMarca($query, $marca)
    {
        if ($marca) {
            return $query->where('marca', 'LIKE', "%$marca%");
        }
    }

    public function scopeOrden($query, $orden)
    {
        if ($orden)
            return $query->orderBy('precio', "$orden");
    }




    public function scopeMin($query, $min)
    {
        if ($min != 0)
            return $query->where('precio', '>=', $min);
    }

    public function scopeMax($query, $max)
    {
        if ($max != 0)
            return $query->where('precio', '<=', $max);
    }
}
